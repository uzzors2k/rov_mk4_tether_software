#!/usr/bin/python
#
# dualshock_ROV_control_serial.py
# Connect a PS3 Dual Shock Controller via Bluetooth
# and read the button states in Python. Print out
# the controller button and analog joystick states.
# Useful for checking the event definitions of each
# input.
#
# Author : Eirik Taylor
# Date   : 13/09/2021
#
#
#=================================

#!/usr/bin/python
import struct
import time
import sys
import signal
import socket
import subprocess
from multiprocessing import Process, Queue

# Controller to read from
joystickPath = "/dev/input/js0"

# Message which defaults all settings on the ROV
defaultMessagePacket = "--"
shutDownMessagePacket = "joystick disconnected"

##################################################################################
# Define events
JS_EVENT_BUTTON = 0x01		# button pressed/released
JS_EVENT_AXIS = 0x02		# joystick moved
JS_EVENT_INIT = 0x80		# initial state of device

JS_BUTTON_ON = 0x01
JS_BUTTON_OFF = 0x00

# Define Dual Axis joystick bindings
# Are these assigned at random on each new machine?
DUAL_SHOCK_LEFT_STICK_HORS = 0
DUAL_SHOCK_LEFT_STICK_VERT = 1
DUAL_SHOCK_RIGHT_STICK_VERT = 4
DUAL_SHOCK_RIGHT_STICK_HORS = 3

DUAL_SHOCK_BUTTON_L1 = 4
DUAL_SHOCK_BUTTON_L2 = 6
DUAL_SHOCK_BUTTON_L3 = 11
DUAL_SHOCK_BUTTON_R1 = 5
DUAL_SHOCK_BUTTON_R2 = 7
DUAL_SHOCK_BUTTON_R3 = 12

DUAL_SHOCK_BUTTON_SELECT = 8
DUAL_SHOCK_BUTTON_START = 9
DUAL_SHOCK_BUTTON_PS = 10

DUAL_SHOCK_BUTTON_DIR_UP = 13
DUAL_SHOCK_BUTTON_DIR_DOWN = 14
DUAL_SHOCK_BUTTON_DIR_LEFT = 15
DUAL_SHOCK_BUTTON_DIR_RIGHT = 16

DUAL_SHOCK_BUTTON_DIR_TRIANGLE = 2
DUAL_SHOCK_BUTTON_DIR_CIRCLE = 1
DUAL_SHOCK_BUTTON_DIR_CROSS = 0
DUAL_SHOCK_BUTTON_DIR_SQUARE = 3

##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
	print 'Shutting down...\n'
	sys.exit(0)
	
signal.signal(signal.SIGINT, handler)


##################################################################################

def limitAndConvertToFloatRatio(input, base):
	# Limit to [1.0, -1.0]
	if (input > base):
		input = base
	elif (input < -base):
		input = -base
	
	# Convert an input to a float between 0.0 and 1.0
	return float(input) / float(base)

##################################################################################

def parseJoystickMovements(messageOutQueue):
	# Various settings
	Analog_stick_max_range = 512
	
	# Specifics for /dev/input/
	#long int, long int, unsigned short, unsigned short, unsigned int
	FORMAT = 'IhBB'
	EMPTY_STRUCT_STRING = '\x00\x00\x00\x00\x00\x00\x00\x00'
	EVENT_SIZE = struct.calcsize(FORMAT)
	
	joystickConnected = False
	joystickHasBeenConnected = False
	try:
		#open file in binary mode
		in_file = open(joystickPath, "rb")
		joystickConnected = True
		joystickHasBeenConnected = True
	except IOError:
		joystickConnected = False
	
	while True:
		if joystickConnected:
			################# Read from the IO stream ##########################
			try:
				# Read from the joystick stream
				event = in_file.read(EVENT_SIZE)
			except IOError:
				event = EMPTY_STRUCT_STRING
				joystickConnected = False
			(evnt_time, value, type, number) = struct.unpack(FORMAT, event)
				
			# Do something based on the event type
			if type != 0 or value != 0:

				# Filter button events
				if (type == JS_EVENT_BUTTON):
					messagePacket = ("Button event:{} Value{}>".format(number, value))
				
				# Filter axis movements for sticks
				elif (type == JS_EVENT_AXIS):
					messagePacket = ("Axis event:{} Value{}>".format(number, int(Analog_stick_max_range * limitAndConvertToFloatRatio(value, 32767))))
					
				else:
					messagePacket = defaultMessagePacket
				
			else:
				# Events with code, type and value == 0 are "separator" events
				messagePacket = "--"
				
			############ Push the new message onto the queue ####################
			try:
				messageOutQueue.put(messagePacket, False)
			except Queue.Full:
				# Don't really care, new data is more important
				pass
			
			#####################################################################
		else:
			############ Shutdown once connection is lost #######################
			if joystickHasBeenConnected:
				try:
					messageOutQueue.put(shutDownMessagePacket, False)
				except Queue.Full:
					# Don't really care, new data is more important
					pass
			
			else:
			########### Need to try reconnecting... #############################
				try:
					#open file in binary mode
					in_file = open(joystickPath, "rb")
					joystickConnected = True
					joystickHasBeenConnected = True
				except IOError:
					joystickConnected = False
			
##################################################################################

# Check if the bluetooth dongle is initialized

print 'Checking for connected bluetooth devices'

# Execute the command to list connected devices, and collect the result
bluetoothListDevicesCommand='hcitool dev'
process = subprocess.Popen(bluetoothListDevicesCommand.split(), stdout=subprocess.PIPE)
output = process.communicate()[0]
print 'Connected devices: ' + output

# Look for hci0 in the result string
desiredResult='hci0'
result = output.find(desiredResult)

# If not found, then there is no way a PS3 controller can connect. Quit.
if (result == -1):
	print 'No bluetooth device found!'
	sys.exit(0)

# Input update period [s]
button_delay = 0.1

##############################################################

joystickMessageQueue = Queue()
joystickProcess = Process(target = parseJoystickMovements, args=(joystickMessageQueue,))
joystickProcess.start()

##############################################################

# Initialized here so the last packet is remembered
sendPacket = defaultMessagePacket

print 'Starting...'

# Main thread handles the socket connection
while True:

	# Read from the queue until it is empty, we only want the last message received
	while not joystickMessageQueue.empty():
		try:
			sendPacket = joystickMessageQueue.get(False)
		except Queue.Empty:
			pass
	
	# Show what we've got
	print sendPacket
	
	time.sleep(button_delay)
