#!/usr/bin/python
#
# dualshock_ROV_control_serial.py
# Connect a PS3 Dual Shock Controller via Bluetooth
# and read the button states in Python. Print out
# the controller button and analog joystick states.
#
# Author : Eirik Taylor
# Date   : 15/09/2021
#
# SET_VIDEO_RECORD:<0-1>
# SET_LIGHT:<0 to 512>
# 
# ROTATE_ROV_YAW:<-512 to 512>
# 
# TRANSLATE_ROV_X:<-512 to 512>
# TRANSLATE_ROV_Y:<-512 to 512>
# TRANSLATE_ROV_Z:<-512 to 512>
# 
# ROTATE_CAMERA_YAW:<-512 to 512>
# ROTATE_CAMERA_PITCH:<-512 to 512>
#
# SYSTEM_SHUTDOWN:<0-1>
#
#	IO pinning:
#
#	GPIO18 - > Switch
#	GPIO23 - > Status LED
#	GPIO24 - > Recording LED
#
#=================================

#!/usr/bin/python
import struct
import time
import sys
import signal
import subprocess
from multiprocessing import Process, Queue

# Controller to read from
joystickPath = "/dev/input/js0"

##################################################################################
# Define events
JS_EVENT_BUTTON = 0x01		# button pressed/released
JS_EVENT_AXIS = 0x02		# joystick moved
JS_EVENT_INIT = 0x80		# initial state of device

JS_BUTTON_ON = 0x01
JS_BUTTON_OFF = 0x00

# Define Dual Axis joystick bindings
# Are these assigned at random on each new machine?
DUAL_SHOCK_LEFT_STICK_HORS = 0
DUAL_SHOCK_LEFT_STICK_VERT = 1
DUAL_SHOCK_RIGHT_STICK_VERT = 4
DUAL_SHOCK_RIGHT_STICK_HORS = 3

DUAL_SHOCK_BUTTON_L1 = 4
DUAL_SHOCK_BUTTON_L2 = 6
DUAL_SHOCK_BUTTON_L3 = 11
DUAL_SHOCK_BUTTON_R1 = 5
DUAL_SHOCK_BUTTON_R2 = 7
DUAL_SHOCK_BUTTON_R3 = 12

DUAL_SHOCK_BUTTON_SELECT = 8
DUAL_SHOCK_BUTTON_START = 9
DUAL_SHOCK_BUTTON_PS = 10

DUAL_SHOCK_BUTTON_DIR_UP = 13
DUAL_SHOCK_BUTTON_DIR_DOWN = 14
DUAL_SHOCK_BUTTON_DIR_LEFT = 15
DUAL_SHOCK_BUTTON_DIR_RIGHT = 16

DUAL_SHOCK_BUTTON_DIR_TRIANGLE = 2
DUAL_SHOCK_BUTTON_DIR_CIRCLE = 1
DUAL_SHOCK_BUTTON_DIR_CROSS = 0
DUAL_SHOCK_BUTTON_DIR_SQUARE = 3

##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
	print 'Shutting down...\n'
	sys.exit(0)
	
signal.signal(signal.SIGINT, handler)


##################################################################################

def limitAndConvertToFloatRatio(input, base):
	# Limit to [1.0, -1.0]
	if (input > base):
		input = base
	elif (input < -base):
		input = -base
	
	# Convert an input to a float between 0.0 and 1.0
	return float(input) / float(base)

##################################################################################

class ControlPacket:

	def __init__(self):
		self.RecordingState = 0
		self.SystemShutdownRequest = 0
		self.RovTranslateX = 0
		self.RovRotateYaw = 0
		self.RovTranslateY = 0
		self.RovTranslateZ = 0
		self.MainLights = 0
		self.CameraRotateYaw = 0
		self.CameraRotatePitch = 0

	def printContents(self):
		return ("<SET_VIDEO_RECORD:{};SET_LIGHT:{};ROTATE_ROV_YAW:{}; \
					TRANSLATE_ROV_X:{};TRANSLATE_ROV_Y:{}; \
					TRANSLATE_ROV_Z:{};ROTATE_CAMERA_YAW:{}; \
					ROTATE_CAMERA_PITCH:{};SYSTEM_SHUTDOWN:0;>".format(
				self.RecordingState,
				self.MainLights,
				self.RovRotateYaw,
				self.RovTranslateX,
				self.RovTranslateY,
				self.RovTranslateZ,
				self.CameraRotateYaw,
				self.CameraRotatePitch,
				self.SystemShutdownRequest))

##################################################################################

def parseJoystickMovements(messageOutQueue):
	# Various settings
	Analog_stick_max_range = 512
	Reduced_analog_factor = 4
	LED_brightness_step = 128
	Camera_pitch_step = 32
	Slow_movement_modifier = True
	
	# Controller state variables
	controlPacket = ControlPacket()
	
	# Specifics for /dev/input/
	#long int, long int, unsigned short, unsigned short, unsigned int
	FORMAT = 'IhBB'
	EMPTY_STRUCT_STRING = '\x00\x00\x00\x00\x00\x00\x00\x00'
	EVENT_SIZE = struct.calcsize(FORMAT)
	
	joystickConnected = False
	joystickHasBeenConnected = False
	try:
		#open file in binary mode
		in_file = open(joystickPath, "rb")
		joystickConnected = True
		joystickHasBeenConnected = True
	except IOError:
		joystickConnected = False
	
	while True:
		if joystickConnected:
			################# Read from the IO stream ##########################
			try:
				# Read from the joystick stream
				event = in_file.read(EVENT_SIZE)
			except IOError:
				event = EMPTY_STRUCT_STRING
				joystickConnected = False
			(evnt_time, value, type, number) = struct.unpack(FORMAT, event)
				
			# Do something based on the event type
			if type != 0 or value != 0:

				# Filter button events
				if (type == JS_EVENT_BUTTON):

					# ROV LED control
					if (number == DUAL_SHOCK_BUTTON_DIR_TRIANGLE):
						if (value == JS_BUTTON_ON):
							controlPacket.MainLights = controlPacket.MainLights + LED_brightness_step
							#print 'ROV LEDs brighter\n'
							if (controlPacket.MainLights > 512):
								controlPacket.MainLights = 512
					elif (number == DUAL_SHOCK_BUTTON_DIR_CROSS):
						if (value == JS_BUTTON_ON):
							controlPacket.MainLights = controlPacket.MainLights - LED_brightness_step
							#print 'ROV LEDs dimmer\n'
							if (controlPacket.MainLights < 0):
								controlPacket.MainLights = 0
					
					# Camera pitch
					if (number == DUAL_SHOCK_BUTTON_DIR_UP):
						if (value == JS_BUTTON_ON):
							controlPacket.CameraRotatePitch = controlPacket.CameraRotatePitch + Camera_pitch_step
							#print 'Camera tilted higher\n'
							if (controlPacket.CameraRotatePitch > 512):
								controlPacket.CameraRotatePitch = 512
					elif (number == DUAL_SHOCK_BUTTON_DIR_DOWN):
						if (value == JS_BUTTON_ON):
							controlPacket.CameraRotatePitch = controlPacket.CameraRotatePitch - Camera_pitch_step
							#print 'ROV tilted lower\n'
							if (controlPacket.CameraRotatePitch < -512):
								controlPacket.CameraRotatePitch = -512
					
					# Camera yaw
					if (number == DUAL_SHOCK_BUTTON_DIR_RIGHT):
						if (value == JS_BUTTON_ON):
							controlPacket.CameraRotateYaw = controlPacket.CameraRotateYaw + Camera_pitch_step
							#print 'Camera turned right\n'
							if (controlPacket.CameraRotateYaw > 512):
								controlPacket.CameraRotateYaw = 512
					elif (number == DUAL_SHOCK_BUTTON_DIR_LEFT):
						if (value == JS_BUTTON_ON):
							controlPacket.CameraRotateYaw = controlPacket.CameraRotateYaw - Camera_pitch_step
							#print 'ROV turned left\n'
							if (controlPacket.CameraRotateYaw < -512):
								controlPacket.CameraRotateYaw = -512
					
					# Center both camera axis
					if (number == DUAL_SHOCK_BUTTON_SELECT):
						if (value == JS_BUTTON_ON):
							controlPacket.CameraRotatePitch = 0
							controlPacket.CameraRotateYaw = 0
							#print 'Camera centered\n'
							
					# Change the video recording state
					if (number == DUAL_SHOCK_BUTTON_START):
						if (value == JS_BUTTON_ON):
							if (controlPacket.RecordingState == 0):
								controlPacket.RecordingState = 1
								#print 'Recording enabled\n'
							else:
								controlPacket.RecordingState = 0
								#print 'Recording disabled\n'
								
					# Reduce the amplitude of the analog stick movements
					if ((number == DUAL_SHOCK_BUTTON_L2) or (number == DUAL_SHOCK_BUTTON_R2)):
						if (value == JS_BUTTON_ON):
							Slow_movement_modifier = False
							#print 'Normal speed mode\n'
						else:
							Slow_movement_modifier = True
							#print 'Reduced speed mode\n'
				
				# Filter axis movements for sticks
				if (type == JS_EVENT_AXIS):
					# Only allow updates from certain axes
					if ((number == DUAL_SHOCK_LEFT_STICK_VERT) \
						or (number == DUAL_SHOCK_LEFT_STICK_HORS) \
						or (number == DUAL_SHOCK_RIGHT_STICK_VERT) \
						or (number == DUAL_SHOCK_RIGHT_STICK_HORS)):
							
							max_stick = Analog_stick_max_range
							if (Slow_movement_modifier):
								# Reduce speed
								max_stick = max_stick / Reduced_analog_factor
							
							# ROV Yaw control
							if (number == DUAL_SHOCK_RIGHT_STICK_HORS):
								#print 'ROV Yaw\n'
								controlPacket.RovRotateYaw = int(max_stick * limitAndConvertToFloatRatio(value, 32767))
								
							# ROV Forward thrust control
							if (number == DUAL_SHOCK_RIGHT_STICK_VERT):
								#print 'ROV move forward/backward\n'
								controlPacket.RovTranslateX = int(max_stick * limitAndConvertToFloatRatio(value, 32767))
								
							# ROV Upward thrust control
							if (number == DUAL_SHOCK_LEFT_STICK_VERT):
								#print 'ROV move up/down\n'
								controlPacket.RovTranslateZ = int(max_stick * limitAndConvertToFloatRatio(value, 32767))
								
							# ROV Strafe control
							if (number == DUAL_SHOCK_LEFT_STICK_HORS):
								#print 'ROV move up/down\n'
								controlPacket.RovTranslateY = int(max_stick * limitAndConvertToFloatRatio(value, 32767))
								

				# Create a packet from the joystick data
				messagePacket = controlPacket.printContents()
									
			else:
				# Events with code, type and value == 0 are "separator" events
				messagePacket = controlPacket.printContents()
				
			############ Push the new message onto the queue ####################
			try:
				messageOutQueue.put(messagePacket, False)
			except Queue.Full:
				# Don't really care, new data is more important
				pass
			
			#####################################################################
		else:
			############ Shutdown once connection is lost #######################
			if joystickHasBeenConnected:
				try:
					controlPacket.SystemShutdownRequest = 1
					shutDownMessagePacket = controlPacket.printContents()
					messageOutQueue.put(shutDownMessagePacket, False)
				except Queue.Full:
					# Don't really care, new data is more important
					pass
			
			else:
			########### Need to try reconnecting... #############################
				try:
					#open file in binary mode
					in_file = open(joystickPath, "rb")
					joystickConnected = True
					joystickHasBeenConnected = True
				except IOError:
					joystickConnected = False
			
##################################################################################

# Check if the bluetooth dongle is initialized

print 'Checking for connected bluetooth devices'

# Execute the command to list connected devices, and collect the result
bluetoothListDevicesCommand='hcitool dev'
process = subprocess.Popen(bluetoothListDevicesCommand.split(), stdout=subprocess.PIPE)
output = process.communicate()[0]
print 'Connected devices: ' + output

# Look for hci0 in the result string
desiredResult='hci0'
result = output.find(desiredResult)

# If not found, then there is no way a PS3 controller can connect. Quit.
if (result == -1):
	print 'No bluetooth device found!'
	sys.exit(0)

# Input update period [s]
button_delay = 0.1

##############################################################

joystickMessageQueue = Queue()
joystickProcess = Process(target = parseJoystickMovements, args=(joystickMessageQueue,))
joystickProcess.start()

##############################################################

# Initialized here so the last packet is remembered
sendPacket = "----"

print 'Starting...'

# Main thread handles the socket connection
while True:

	# Read from the queue until it is empty, we only want the last message received
	while not joystickMessageQueue.empty():
		try:
			sendPacket = joystickMessageQueue.get(False)
		except Queue.Empty:
			pass
	
	# Show what we've got
	print sendPacket
	time.sleep(button_delay)
