#!/usr/bin/python
#
# serial_test.py
# Open the character device, and issue an ioctl command.
# Read and write to the device.
#
# Author : Eirik Taylor
# Date   : 12/09/2021
#
#=================================

#!/usr/bin/python
import time
import sys
import signal
from fcntl import ioctl
import os
from ctypes import *
import struct

# Character device to read from
charDevicePath = "/dev/rs485_ch"

##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
	print 'Shutting down...\n'
	os.close(fd)
	sys.exit(0)
	
signal.signal(signal.SIGINT, handler)


##################################################################################

# Open connection

fd = os.open(charDevicePath, os.O_RDWR)

#define RS485IOC_SETBAUD 100
RS485IOC_BASE = 2147771292
BAUD_RATE_INDEX = RS485IOC_BASE + 100

print 'Setting baudrate with ioctl'
baudRateValue = 112500
value = c_uint64(baudRateValue)
value2 = struct.pack("L", baudRateValue)

# Both work, pick one

ioctl(fd, BAUD_RATE_INDEX, value2)

print "Sending character over serial line"
os.write(fd, "herro thar!")

print "Reading characters from serial line"
while 1:
	# Read one by one char
	char = os.read(fd, 1)
	if not char:
		break;
		
	print(char)

os.close(fd)

