

Run "install_1_PS3_controller.sh" first

Next, plug in the PS3 controller and set it's master by running

cd sixxaxis/utils/bins/
sudo ./sixpair


Finally, the PS3 controller has to be paired to the Raspberry Pi


A guide is here https://www.piborg.org/rpi-ps3-help or just follow the instructions below

Make the internal bluetooth unit discoverable to other units, and add the PS3 controller to the trusted units.

sudo bluetoothctl
discoverable on
agent on


Now you can press the PS button on the controller and it should attempt to talk to the Raspberry Pi.
You should see some log lines like this at a regular interval:

[NEW] Device 38:C0:96:5C:C6:60 38-C0-96-5C-C6-60
[CHG] Device 38:C0:96:5C:C6:60 Connected: no
[DEL] Device 38:C0:96:5C:C6:60 38-C0-96-5C-C6-60


You will need to make a note of the MAC address displayed, it is the sequence with ':' symbols.
In this example it is 38:C0:96:5C:C6:60
With this we can attempt to make contact with the controller.
We need to use the connect command with the MAC address shown, in our example:

connect 38:C0:96:5C:C6:60

You are trying to get the Bluetooth to try a connection and get a UUID number.
This may take several attempts, you can repeat the command using ↑ then ENTER.
When it works you should see something like this:

Attempting to connect to 38:C0:96:5C:C6:60
[CHG] Device 38:C0:96:5C:C6:60 Modalias: usb:v054Cp0268d0100
[CHG] Device 38:C0:96:5C:C6:60 UUIDs:
        00001124-0000-1000-8000-00805f9b34fb
        00001200-0000-1000-8000-00805f9b34fb
Failed to connect: org.bluez.Error.Failed

If the controller stops trying to connect press the PS button again before using the connect command again.
Once we have seen the UUID values we can use the trust command to allow the controller to connect on its own.
Use the trust command with the MAC address from earlier, in our example:

trust 38:C0:96:5C:C6:60

If everything went well you should see something like:

[CHG] Device 38:C0:96:5C:C6:60 Trusted: yes
Changing 38:C0:96:5C:C6:60 trust succeeded

Finally exit the Bluetooth configuration tool and restart the Raspberry Pi.

quit
sudo reboot


