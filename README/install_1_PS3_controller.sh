#!/bin/bash

# Find the latest repositories
sudo apt-get update

# Update Bluetooth
sudo apt-get install bluez
sudo apt-get install libusb-dev
sudo apt-get install libbluetooth3
sudo apt-get install libbluetooth3-dev

# Build the six axis controller software
cd ..
cd sixxaxis/
make
sudo make install
cd ..

