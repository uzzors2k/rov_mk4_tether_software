Welcome!

Some initial first steps are needed to setup the raspberry pi 3 from this image.

From raspi-config, also setup the following:

1) System Options -> Hostname -> Set to rovtether or rovcore
2) System Options -> Wait for Network on boot -> No
3) Advanced Options -> Expand Filesystem

