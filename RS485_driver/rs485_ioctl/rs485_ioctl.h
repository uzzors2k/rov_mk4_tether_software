#include <linux/ioctl.h>

// TODO:    Should pick something more pro
#define RS485CH_IOCTL_BASE 100
#define RS485IOC_SETBAUD    _IOR(RS485CH_IOCTL_BASE, 0, int)
#define RS485IOC_SETSPICLK  _IOR(RS485CH_IOCTL_BASE + 1, 0, int)
#define RS485IOC_FLUSH      _IOR(RS485CH_IOCTL_BASE + 2, 0, int)
