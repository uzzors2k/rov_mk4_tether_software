#!/usr/bin/python
#
# dualshock_ROV_control_serial.py
# Connect a PS3 Dual Shock Controller via Bluetooth
# and read the button states in Python. Print out
# the controller button and analog joystick states.
#
# Author : Eirik Taylor
# Date   : 15/09/2021
#
#    IO pinning:
#
#    GPIO21 - > Switch
#    GPIO26 - > Status LED
#    GPIO20 - > Recording LED
#
#=================================

import struct
import time
import sys
import signal
import subprocess
from subprocess import call
from multiprocessing import Process, Queue
from fcntl import ioctl
import os
from ctypes import *
import struct
import RPi.GPIO as GPIO

# Controller to read from
joystickPath = "/dev/input/js0"

# Output device to send serial characters on
charDevicePath = "/dev/rs485_ch"

switchGpioNumber = 21
greenLedGpioNumber = 26
redLedGpioNumer = 20

##################################################################################
# Define events
JS_EVENT_BUTTON = 0x01        # button pressed/released
JS_EVENT_AXIS = 0x02        # joystick moved
JS_EVENT_INIT = 0x80        # initial state of device

JS_BUTTON_ON = 0x01
JS_BUTTON_OFF = 0x00

# Define Dual Axis joystick bindings
# Are these assigned at random on each new machine?
DUAL_SHOCK_LEFT_STICK_HORS = 0
DUAL_SHOCK_LEFT_STICK_VERT = 1
DUAL_SHOCK_RIGHT_STICK_VERT = 4
DUAL_SHOCK_RIGHT_STICK_HORS = 3

DUAL_SHOCK_BUTTON_L1 = 4
DUAL_SHOCK_BUTTON_L2 = 6
DUAL_SHOCK_BUTTON_L3 = 11
DUAL_SHOCK_BUTTON_R1 = 5
DUAL_SHOCK_BUTTON_R2 = 7
DUAL_SHOCK_BUTTON_R3 = 12

DUAL_SHOCK_BUTTON_SELECT = 8
DUAL_SHOCK_BUTTON_START = 9
DUAL_SHOCK_BUTTON_PS = 10

DUAL_SHOCK_BUTTON_DIR_UP = 13
DUAL_SHOCK_BUTTON_DIR_DOWN = 14
DUAL_SHOCK_BUTTON_DIR_LEFT = 15
DUAL_SHOCK_BUTTON_DIR_RIGHT = 16

DUAL_SHOCK_BUTTON_DIR_TRIANGLE = 2
DUAL_SHOCK_BUTTON_DIR_CIRCLE = 1
DUAL_SHOCK_BUTTON_DIR_CROSS = 0
DUAL_SHOCK_BUTTON_DIR_SQUARE = 3

##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def cleanupResourcesAndExit():
    print 'Shutting down...\n'
    os.close(fd)
    GPIO.cleanup()
    sys.exit(0)


##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
    cleanupResourcesAndExit()

signal.signal(signal.SIGINT, handler)


##################################################################################

def shutdownEverything():
	command = "/usr/bin/sudo /sbin/shutdown -h now"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	print output
	cleanupResourcesAndExit()

##################################################################################

def limitAndConvertToFloatRatio(input, base):
    # Limit to [1.0, -1.0]
    if (input > base):
        input = base
    elif (input < -base):
        input = -base

    # Convert an input to a float between 0.0 and 1.0
    return float(input) / float(base)
    
####################### RS-485 Interface #######################################

def setBaudrate(baudrateToSet):
    #define RS485IOC_SETBAUD 100
    setIntWithIoctl(2147771392, baudrateToSet)

def setSpiClkRate(spiClkRateToSet):
    #define RS485IOC_SETBAUD 101
    setIntWithIoctl(2147771648, spiClkRateToSet)

def flushBuffers():
    #define RS485IOC_FLUSH 102
    setIntWithIoctl(2147771904, 0)

def setIntWithIoctl(index, intData):
    dataAsMemoryObject = struct.pack("L", intData)
    ioctl(fd, index, dataAsMemoryObject)
    print("Setting ioctl index %d with data %d" % (index, intData))

##################################################################################

class ControlPacket:

    def __init__(self):
        self.RecordingState = 0
        self.SystemShutdownRequest = 0
        self.RovTranslateX = 0
        self.RovRotateYaw = 0
        self.RovTranslateY = 0
        self.RovTranslateZ = 0
        self.MainLights = 0
        self.CameraRotateYaw = 0
        self.CameraRotatePitch = 0

    def getEntireState(self):
        return ("<SET_VIDEO_RECORD:{};SET_LIGHT:{};ROTATE_ROV_YAW:{}; \
            TRANSLATE_ROV_X:{};TRANSLATE_ROV_Y:{}; \
            TRANSLATE_ROV_Z:{};ROTATE_CAMERA_YAW:{}; \
            ROTATE_CAMERA_PITCH:{};SYSTEM_SHUTDOWN:0;>".format(
                self.RecordingState,
                self.MainLights,
                self.RovRotateYaw,
                self.RovTranslateX,
                self.RovTranslateY,
                self.RovTranslateZ,
                self.CameraRotateYaw,
                self.CameraRotatePitch,
                self.SystemShutdownRequest))

    def getSerialPacket(self):
        # Parse into framed serial message
        # Should maybe add error correction as well
        Start_flag = 0x12
        End_flag = 0x13
        Escape_flag = 0x7D

        serialDataPacket = [
            self.RecordingState,
            self.SystemShutdownRequest,
            self.MainLights,
            self.RovTranslateX + 127,
            self.RovTranslateZ + 127,
            self.RovRotateYaw + 127]

        # Add escape characters to data
        i = 0
        while i < len(serialDataPacket):
            if (serialDataPacket[i] == Start_flag) or \
            (serialDataPacket[i] == End_flag) or \
            (serialDataPacket[i] == Escape_flag) :
                serialDataPacket.insert(i, Escape_flag)
                i += 2
            else :
                i += 1

        # Insert start and end flags
        serialDataPacket.insert(0, Start_flag)
        serialDataPacket.insert(len(serialDataPacket), End_flag)

        return serialDataPacket

##################################################################################

def parseJoystickMovements(messageOutQueue):

    # Various settings
    LED_setting_max_range = 255
    LED_setting_min_range = 0
    Analog_stick_max_range = 127
    Reduced_analog_factor = 4
    LED_brightness_step = 64
    Camera_pitch_step = 8
    Slow_movement_modifier = True

    # Controller state variables
    controlPacket = ControlPacket()

    # Specifics for /dev/input/
    #long int, long int, unsigned short, unsigned short, unsigned int
    FORMAT = 'IhBB'
    EMPTY_STRUCT_STRING = '\x00\x00\x00\x00\x00\x00\x00\x00'
    EVENT_SIZE = struct.calcsize(FORMAT)

    joystickConnected = False
    joystickHasBeenConnected = False
    try:
        #open file in binary mode
        in_file = open(joystickPath, "rb")
        joystickConnected = True
        joystickHasBeenConnected = True
    except IOError:
        joystickConnected = False

    while True:
        if joystickConnected:
            ################# Read from the IO stream ##########################
            try:
                # Read from the joystick stream
                event = in_file.read(EVENT_SIZE)
            except IOError:
                event = EMPTY_STRUCT_STRING
                joystickConnected = False
            (evnt_time, value, type, number) = struct.unpack(FORMAT, event)

            # Do something based on the event type
            if type != 0 or value != 0:

                # Filter button events
                if (type == JS_EVENT_BUTTON):

                    # ROV LED control
                    if (number == DUAL_SHOCK_BUTTON_DIR_TRIANGLE):
                        if (value == JS_BUTTON_ON):
                            controlPacket.MainLights = controlPacket.MainLights + LED_brightness_step
                            #print 'ROV LEDs brighter\n'
                            if (controlPacket.MainLights > LED_setting_max_range):
                                controlPacket.MainLights = LED_setting_max_range
                    elif (number == DUAL_SHOCK_BUTTON_DIR_CROSS):
                        if (value == JS_BUTTON_ON):
                            controlPacket.MainLights = controlPacket.MainLights - LED_brightness_step
                            #print 'ROV LEDs dimmer\n'
                            if (controlPacket.MainLights < LED_setting_min_range):
                                controlPacket.MainLights = LED_setting_min_range

                    # Camera pitch
                    if (number == DUAL_SHOCK_BUTTON_DIR_UP):
                        if (value == JS_BUTTON_ON):
                            controlPacket.CameraRotatePitch = controlPacket.CameraRotatePitch + Camera_pitch_step
                            #print 'Camera tilted higher\n'
                            if (controlPacket.CameraRotatePitch > Analog_stick_max_range):
                                controlPacket.CameraRotatePitch = Analog_stick_max_range
                    elif (number == DUAL_SHOCK_BUTTON_DIR_DOWN):
                        if (value == JS_BUTTON_ON):
                            controlPacket.CameraRotatePitch = controlPacket.CameraRotatePitch - Camera_pitch_step
                            #print 'ROV tilted lower\n'
                            if (controlPacket.CameraRotatePitch < -Analog_stick_max_range):
                                controlPacket.CameraRotatePitch = -Analog_stick_max_range

                    # Camera yaw
                    if (number == DUAL_SHOCK_BUTTON_DIR_RIGHT):
                        if (value == JS_BUTTON_ON):
                            controlPacket.CameraRotateYaw = controlPacket.CameraRotateYaw + Camera_pitch_step
                            #print 'Camera turned right\n'
                            if (controlPacket.CameraRotateYaw > Analog_stick_max_range):
                                controlPacket.CameraRotateYaw = Analog_stick_max_range
                    elif (number == DUAL_SHOCK_BUTTON_DIR_LEFT):
                        if (value == JS_BUTTON_ON):
                            controlPacket.CameraRotateYaw = controlPacket.CameraRotateYaw - Camera_pitch_step
                            #print 'ROV turned left\n'
                            if (controlPacket.CameraRotateYaw < -Analog_stick_max_range):
                                controlPacket.CameraRotateYaw = -Analog_stick_max_range

                    # Center both camera axis
                    if (number == DUAL_SHOCK_BUTTON_SELECT):
                        if (value == JS_BUTTON_ON):
                            controlPacket.CameraRotatePitch = 0
                            controlPacket.CameraRotateYaw = 0
                            #print 'Camera centered\n'

                    # Change the video recording state
                    if (number == DUAL_SHOCK_BUTTON_START):
                        if (value == JS_BUTTON_ON):
                            if (controlPacket.RecordingState == 0):
                                controlPacket.RecordingState = 1
                            else:
                                controlPacket.RecordingState = 0

                    # Reduce the amplitude of the analog stick movements
                    if ((number == DUAL_SHOCK_BUTTON_L2) or (number == DUAL_SHOCK_BUTTON_R2)):
                        if (value == JS_BUTTON_ON):
                            Slow_movement_modifier = False
                            #print 'Normal speed mode\n'
                        else:
                            Slow_movement_modifier = True
                            #print 'Reduced speed mode\n'

                # Filter axis movements for sticks
                if (type == JS_EVENT_AXIS):
                    # Only allow updates from certain axes
                    if ((number == DUAL_SHOCK_LEFT_STICK_VERT) \
                        or (number == DUAL_SHOCK_LEFT_STICK_HORS) \
                        or (number == DUAL_SHOCK_RIGHT_STICK_VERT) \
                        or (number == DUAL_SHOCK_RIGHT_STICK_HORS)):

                            max_stick = Analog_stick_max_range
                            if (Slow_movement_modifier):
                                # Reduce speed
                                max_stick = max_stick / Reduced_analog_factor

                            # ROV Yaw control
                            if (number == DUAL_SHOCK_RIGHT_STICK_HORS):
                                #print 'ROV Yaw\n'
                                controlPacket.RovRotateYaw = int(max_stick * limitAndConvertToFloatRatio(value, 32767))

                            # ROV Forward thrust control
                            if (number == DUAL_SHOCK_RIGHT_STICK_VERT):
                                #print 'ROV move forward/backward\n'
                                controlPacket.RovTranslateX = int(max_stick * limitAndConvertToFloatRatio(value, 32767))

                            # ROV Upward thrust control
                            if (number == DUAL_SHOCK_LEFT_STICK_VERT):
                                #print 'ROV move up/down\n'
                                controlPacket.RovTranslateZ = int(max_stick * limitAndConvertToFloatRatio(value, 32767))

                            # ROV Strafe control
                            if (number == DUAL_SHOCK_LEFT_STICK_HORS):
                                #print 'ROV move up/down\n'
                                controlPacket.RovTranslateY = int(max_stick * limitAndConvertToFloatRatio(value, 32767))


            #else:
                # Events with code, type and value == 0 are "separator" events

            ############ Push the new message onto the queue ####################
            try:
                messageOutQueue.put(controlPacket, False)
            except Queue.Full:
                # Don't really care, new data is more important
                pass

            #####################################################################
        else:
            ############ Shutdown once connection is lost #######################
            if joystickHasBeenConnected:
                try:
                    controlPacket.SystemShutdownRequest = 1
                    messageOutQueue.put(controlPacket, False)
                except Queue.Full:
                    # Don't really care, new data is more important
                    pass

            else:
            ########### Need to try reconnecting... #############################
                try:
                    #open file in binary mode
                    in_file = open(joystickPath, "rb")
                    joystickConnected = True
                    joystickHasBeenConnected = True
                except IOError:
                    joystickConnected = False

##################################################################################

# Check if the bluetooth dongle is initialized

print 'Checking for connected bluetooth devices'

# Execute the command to list connected devices, and collect the result
bluetoothListDevicesCommand='hcitool dev'
process = subprocess.Popen(bluetoothListDevicesCommand.split(), stdout=subprocess.PIPE)
output = process.communicate()[0]
print 'Connected devices: ' + output

# Look for hci0 in the result string
desiredResult='hci0'
result = output.find(desiredResult)

# If not found, then there is no way a PS3 controller can connect. Quit.
if (result == -1):
    print 'No bluetooth device found!'
    sys.exit(0)

##############################################################

# Attempt to initialize the serial device
fd = os.open(charDevicePath, os.O_RDWR)

print 'Setting RS-485 parameters with ioctl'
setBaudrate(112500)
setSpiClkRate(18000000)
flushBuffers()

##############################################################

# Setup GPIO pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(switchGpioNumber, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(greenLedGpioNumber, GPIO.OUT)
GPIO.setup(redLedGpioNumer, GPIO.OUT)

GPIO.output(greenLedGpioNumber, False)
GPIO.output(redLedGpioNumer, False)

##############################################################

joystickMessageQueue = Queue()
joystickProcess = Process(target = parseJoystickMovements, args=(joystickMessageQueue,))
joystickProcess.start()

##############################################################

print 'Waiting for Dualshock Controller to connect...'
GPIO.output(greenLedGpioNumber, True)

# Input update period [s]
button_delay = 0.1
serialClientConnected = False
sendPacket = ControlPacket()
currentRecordingState = 0

# Main thread handles the socket connection
while True:

    # Read from the queue until it is empty, we only want the last message received
    while not joystickMessageQueue.empty():
        try:
            sendPacket = joystickMessageQueue.get(False)

            # Check client connection Status
            if not serialClientConnected :
                serialClientConnected = True
                print ("Connected to client")

        except Queue.Empty:
            pass

    # Check if the shutdown button has been pressed
    if ((GPIO.input(switchGpioNumber) == 0) or (sendPacket.SystemShutdownRequest != 0)):
        print ("Shutdown requested!")
        break
    elif (sendPacket.RecordingState != currentRecordingState):
            currentRecordingState = sendPacket.RecordingState
            if (currentRecordingState == 1):
                GPIO.output(redLedGpioNumer, True)
            else:
                GPIO.output(redLedGpioNumer, False)

    # Show what we've got
    print ("Control packet: {}".format(sendPacket.getEntireState()))
    os.write(fd, bytearray(sendPacket.getSerialPacket()))
    time.sleep(button_delay)

################################################################
# Landing here means the loop has been broken, exit!
print ("Sending shutdown packet to client")
terminationPacket = ControlPacket()
terminationPacket.SystemShutdownRequest = 1
terminationPacketMsg = terminationPacket.getSerialPacket()

for x in range(3):
    print ("Serial data: {}".format(terminationPacketMsg))
    os.write(fd, bytearray(terminationPacketMsg))
    time.sleep(button_delay)

joystickProcess.terminate()
joystickProcess.join()

#cleanupResourcesAndExit()   # Debugging, only exit the script
shutdownEverything()
