# README #

Control software for an ROV tether unit. Communication with the ROV is over a RS-485 line. Script is intended to run at boot time. Video feed is sent via an analog system apart from the tether Raspberry Pi unit.

### How do I get set up? ###

* This software was made specifically to run on a raspberry pi 3, but should be adaptable to other platforms. Should work more out of the box, follow the instructions in the README folder to set up.

### Contribution guidelines ###

* Feel free to come with suggestions! Currently looking into sending video feed over the RS-485 link, to eliminate the analog video system.

### Who do I talk to? ###

* Eirik Taylor @ uzzors2k.com
